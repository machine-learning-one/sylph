import slugify from 'rehype-slug';

const config = {
	extensions: ['.md'],

	smartypants: {
		dashes: 'oldschool'
	},

	remarkPlugins: [],
	rehypePlugins: [slugify],
	layout: {
		post: './src/lib/layouts/post.svelte',
		page: './src/lib/layouts/page.svelte'
	}
};

export default config;
