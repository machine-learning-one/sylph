# sylph

Opinionated Sveltekit Template

- SvelteKit, Yarn
- Tailwindcss and cssnano
- Markdown Support
- Pre-rendering
- Typescript
- [ImageTools Plugin](https://github.com/JonasKruckenberg/imagetools)
