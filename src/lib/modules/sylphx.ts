import { writable } from 'svelte/store';
import { browser } from '$app/env';

export function continuous(name: string, def: string): writable {
	const store = writable((browser && localStorage.getItem(name)) || def);
	if (browser) {
		store.subscribe((val) => localStorage.setItem(name, val));
	}
	return store;
}

export function session(name: string, def: string): writable {
	const store = writable((browser && sessionStorage.getItem(name)) || def);
	if (browser) {
		store.subscribe((val) => sessionStorage.setItem(name, val));
	}
	return store;
}
