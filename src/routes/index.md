---
layout: post
---

<script lang="ts">
    import Counter from "$lib/components/counter.svelte"
    import { continuous as store } from "$lib/modules/sylphx"
    const p = store('sylph','Sylph supports persistent stores')
</script>

# Welcome to Sylph

## Extended Stores

<input type="text" bind:value={$p}/>

### Variants

supports continuous (long-term) and session storage

## Example Components

<Counter />

## Further Readings

Visit [kit.svelte.dev](https://kit.svelte.dev 'The best framework for interactive blogs') to read the documentation
